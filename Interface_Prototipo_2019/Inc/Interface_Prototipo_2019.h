/*
 * Interface_Prototipo_2019.h
 *
 *  Created on: 29 de ago de 2019
 *      Author: Formula Tesla UFMG
 *  
 * 
 *  Identificadores Rede CAN: https://docs.google.com/spreadsheets/d/1wuYIXwkoI3ViJUeTy1g0zmYpwM5OYqyshXtDRXcwHPk/edit#gid=1354529818
 * 
 */

#ifndef INTERFACE_PROTOTIPO_2019_H_
#define INTERFACE_PROTOTIPO_2019_H_

/* Includes */
#include "stm32f1xx_hal.h"
#include "DMA_USART.h"
#include "nextion.h"
#include "xbee.h"
#include "can.h"

/* Defines */
#define CAN_IDS_NUMBER 400
#define REAL_CLK_CAN_ID 10

/* TADs */
/* CAN message struct */
typedef struct{
  uint16_t word_0;
  uint16_t word_1;
  uint16_t word_2;
  uint16_t word_3;
}CanIdData_t;

/* Timer struct */
typedef struct{
  uint32_t previous;
  uint16_t interval;
}Timer_t;

/* Nextion PAGE Enum */
typedef enum {
    PAGE0,
    PAGE1,
    PAGE2
}NextionPage_e;

/* Nextion Page 0 data */
typedef struct {
    uint16_t actual;
    uint16_t previous;
}NextionData_t;

/*Functions*/
void uart2MessageReceived(void);
void realClockRequest(void);
void telemetrySend(SendMode_e mode);

/* Panel Functions */
void uart3MessageReceived(void);
void nextionLoop(void);
void nextionTestLoop(void);

/* Other Functions */
void UART_Print_Debug(char* format, ...);
void interfaceInit(void);
void canMessageReceived(uint16_t id, uint8_t* data);
void blinkLed(void);
void timerAtualization(void);
void debugFunction(void);

#endif  /* INTERFACE_PROTOTIPO_2019_H_ */
