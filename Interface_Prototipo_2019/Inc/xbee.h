/*
 * xbee.h
 *
 *  Created on: 8 de ago de 2019
 *      Author: Formula Tesla UFMG
 */

#ifndef XBEE_H_
#define XBEE_H_

#include <stdarg.h>
#include "stm32f1xx_hal.h"
#include "usart.h"
#include <string.h>

/*Defines*/
#define DELAY_XBEE 0

/*XBEE MODE ENUM*/
typedef enum {
  BYTES_API,
  STRING_API,
  STRING_TRANSPARENT,
  BYTES_TRANSPARENT
}SendMode_e;

/* Functions */
uint8_t xbeeApiModeSend(char *xbeeBuffer, int buff_size);
void xbeeSend(int id, SendMode_e mode, ...);

#endif /* XBEE_H_ */
