/*
 * Interface_Prototipo_2019.c
 *
 *  Created on: 29 de ago de 2019
 *      Author: Formula Tesla UFMG
 *  
 * 
 *  Identificadores Rede CAN: https://docs.google.com/spreadsheets/d/1wuYIXwkoI3ViJUeTy1g0zmYpwM5OYqyshXtDRXcwHPk/edit#gid=1354529818
 * 
 */

#include <Interface_Prototipo_2019.h>

/*Timers variables*/
Timer_t packTimer;
Timer_t ledTimer;
uint32_t actualTimer;

/* CAN data struct vector */
CanIdData_t can_vector[CAN_IDS_NUMBER];

/* DMA and UARTs structs */
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart3_rx;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern uint8_t uart_user_message[DMA_RX_BUFFER_SIZE];	/* Buffer received for user access */

/*Nextion variables*/
NextionPage_e actual_page = PAGE0;
NextionData_t speed;
NextionData_t battery_percent;
NextionData_t torq_rotation;
int auxiliar = 0;

/* Telemetry variables */
uint8_t _real_clock_received = 0;



/* Functions */

uint8_t compareString(uint8_t *first, uint8_t *second, uint16_t len) {
  while (*first == *second) {

    len--;
    if(len == 0)  return 1; /* Same strings */
     
    first++;
    second++;
  }

  return 0;
}

/* Telemetry functions */
void uart2MessageReceived(void)
{
  uint16_t* data_word;
  uint16_t checksum = 0;
  uint8_t return_status = 0;
  const uint8_t* ping_request = "ping";
  
  if(uart_user_message[3] != 0x90)  return; /* If the message received != "Receive Packet" */
  //UART_Print_Debug("Mensagem 0x90 recebida\r\n");

  if(_real_clock_received)  return;

  return_status = compareString(uart_user_message + 15, ping_request, 4); /* Comparing if the received message is the xbee ping request */
  if(return_status) return;
  UART_Print_Debug("Mensagem xbee recebida\r\n");

  uint16_t api_length = uart_user_message[1] << 8;
  api_length += uart_user_message[2];
  UART_Print_Debug("api_length = %d\r\n", api_length);
  if(api_length > 255)  return; /* If api message length > DMA_RX_BUFFER_SIZE */

  for(uint16_t i = 3; i < (api_length + 4); i++)  checksum += uart_user_message[i];

  if(checksum & 0xFF == 0xFF)
  {
    UART_Print_Debug("\r\n");
    UART_Print_Debug("Mensagem Recebida:\n\r");
    for(uint8_t i = 3; i < api_length + 4; i++)	UART_Print_Debug("%u\n", uart_user_message[i]);
    UART_Print_Debug("\r\n");

    can_vector[REAL_CLK_CAN_ID].word_0 = uart_user_message[15] << 8;
    can_vector[REAL_CLK_CAN_ID].word_0 = uart_user_message[16];

    can_vector[REAL_CLK_CAN_ID].word_1 = uart_user_message[17] << 8;
    can_vector[REAL_CLK_CAN_ID].word_1 = uart_user_message[18];

    can_vector[REAL_CLK_CAN_ID].word_2 = uart_user_message[19] << 8;
    can_vector[REAL_CLK_CAN_ID].word_2 = uart_user_message[20];

    can_vector[REAL_CLK_CAN_ID].word_3 = uart_user_message[21] << 8;
    can_vector[REAL_CLK_CAN_ID].word_3 = uart_user_message[22];
    
    _real_clock_received = 1;
    can_vector[REAL_CLK_CAN_ID].word_1 = 1;
    UART_Print_Debug("_realclock received na interrupção\n\r");
  }
}


void realClockRequest(void)
{
  UART_Print_Debug("Entrei na funcao reaclock\r\n");
  uint8_t can_vet_tx[8];

  /* 1) Global variables Init */
  can_vector[REAL_CLK_CAN_ID].word_0 = 1;
  can_vector[REAL_CLK_CAN_ID].word_1 = 0;
  can_vector[REAL_CLK_CAN_ID].word_2 = 0;
  can_vector[REAL_CLK_CAN_ID].word_3 = 0;

  /* 2) Sending clock request */
  for(uint8_t i = 0; i < 40; i++){

    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    HAL_Delay(50);

    if(_real_clock_received == 1){
      UART_Print_Debug("_realclock received, por isso to saindo da requisição\n\r");
      UART_Print_Debug("Número de tentativas = %u\n\r", i);
    }
    else{
      xbeeSend(REAL_CLK_CAN_ID, BYTES_API, can_vector[REAL_CLK_CAN_ID].word_0, can_vector[REAL_CLK_CAN_ID].word_1, can_vector[REAL_CLK_CAN_ID].word_2, can_vector[REAL_CLK_CAN_ID].word_3);
    }
  }

  /* Sending the real clock in can bus */
  can_vet_tx[0] = can_vector[REAL_CLK_CAN_ID].word_0;
  can_vet_tx[1] = can_vector[REAL_CLK_CAN_ID].word_0 >> 8;
  can_vet_tx[2] = can_vector[REAL_CLK_CAN_ID].word_1;
  can_vet_tx[3] = can_vector[REAL_CLK_CAN_ID].word_1 >> 8;
  can_vet_tx[4] = can_vector[REAL_CLK_CAN_ID].word_2;
  can_vet_tx[5] = can_vector[REAL_CLK_CAN_ID].word_2 >> 8;
  can_vet_tx[6] = can_vector[REAL_CLK_CAN_ID].word_3;
  can_vet_tx[7] = can_vector[REAL_CLK_CAN_ID].word_3 >> 8;
  canTransmit(can_vet_tx, REAL_CLK_CAN_ID);
}

void xbeePacks(SendMode_e mode)
{
  uint16_t i;

  /*Pack 0*/
  for(i = 260; i < 264; i++)
    xbeeSend(i, mode, can_vector[i].word_0, can_vector[i].word_1, can_vector[i].word_2, can_vector[i].word_3);

  /*Pack 1*/
  for(i = 270; i < 274; i++)
    xbeeSend(i, mode, can_vector[i].word_0, can_vector[i].word_1, can_vector[i].word_2, can_vector[i].word_3);

  /*Pack 2*/
  for(i = 280; i < 284; i++)
    xbeeSend(i, mode, can_vector[i].word_0, can_vector[i].word_1, can_vector[i].word_2, can_vector[i].word_3);

  /*Pack 3*/
  for(i = 290; i < 294; i++)
    xbeeSend(i, mode, can_vector[i].word_0, can_vector[i].word_1, can_vector[i].word_2, can_vector[i].word_3);
}
void xbeeGeneral(SendMode_e mode)
{
  uint16_t i;

	/*Bateria General Information*/
	xbeeSend(0, mode, can_vector[0].word_0, 0, 0, 0);
  for(i = 51; i < 55; i++)
    xbeeSend(i, mode, can_vector[i].word_0, can_vector[i].word_1, can_vector[i].word_2, can_vector[i].word_3);

	/*Aquisição General Information*/
  xbeeSend(161, mode, can_vector[161].word_0, can_vector[161].word_1, can_vector[161].word_2, can_vector[161].word_3);
  xbeeSend(170, mode, can_vector[170].word_0, can_vector[170].word_1, can_vector[170].word_2, can_vector[170].word_3);
  xbeeSend(171, mode, can_vector[171].word_0, can_vector[171].word_1, can_vector[171].word_2, can_vector[171].word_3);
  
	/*Controle General Information*/
  for(i = 100; i < 108; i++)
    xbeeSend(i, mode, can_vector[i].word_0, can_vector[i].word_1, can_vector[i].word_2, can_vector[i].word_3);
}
void telemetrySend(SendMode_e mode)
{
  if((actualTimer-packTimer.previous)>packTimer.interval){
    xbeePacks(mode);
    packTimer.previous = HAL_GetTick();
  }
  xbeeGeneral(mode);
}

/* Panel Functions */
void uart3MessageReceived(void)
{
  /* If the message is to change the nextion page */
  if(uart_user_message[0] == 0x71 && uart_user_message[5] == 0xFF && uart_user_message[6] == 0xFF && uart_user_message[7] == 0xFF)
  {
    //UART_Print_Debug("UART3:\r\n");
    //HAL_UART_Transmit(&huart1, uart_user_message, strlen(uart_user_message), 10);
	  HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);

    switch(uart_user_message[1])
    {
      case 0:
      actual_page = PAGE0;
      NexPageShow(PAGE0);
      break;

      case 1:
      actual_page = PAGE1;
      NexPageShow(PAGE1);
      break;

      case 2:
      actual_page = PAGE2;
      NexPageShow(PAGE2);
    }
  }
}
void nextionLoop(void)
{
  switch(actual_page)
  {
    case PAGE0:

    //speed.actual = can_vector[].word_;
    if(speed.actual != speed.previous){
      NexNumberSetValue(0, speed.actual);
      speed.previous = speed.actual;
    }

    //battery_percent.actual = can_vector[52].word_1;
    if(battery_percent.actual != battery_percent.previous){
      NexProgressBarSetValue(0, battery_percent.actual);
      battery_percent.previous = battery_percent.actual;
    }

    //torq_rotation.actual = can_vector[].word_;
    if(torq_rotation.actual != torq_rotation.previous){
      NexProgressBarSetValue(1, torq_rotation.actual);
      torq_rotation.previous = torq_rotation.actual;
    }
    break;
    
    case PAGE1:

    NexNumberSetValue(0, auxiliar);
    NexNumberSetValue(1, auxiliar);
    NexNumberSetValue(2, auxiliar);
    NexTextSetText(0, "aberto");
    NexTextSetText(1, "aberto");
    NexTextSetText(2, "aberto");
    break;

    case PAGE2:

    NexNumberSetValue(0, can_vector[53].word_0);  /* Max Temperature */
    NexNumberSetValue(3, can_vector[53].word_2);  /* Avarage Temperature */
    NexNumberSetValue(1, can_vector[54].word_0);  /* Minimum Voltage */
    if(can_vector[0].word_0 == 0) NexTextSetText(0, "aberto");  /* AIR Status */
    else  NexTextSetText(0, "fechado");
    NexNumberSetValue(2, can_vector[53].word_1);  /* Total Voltage */
    NexNumberSetValue(4, can_vector[52].word_0);  /* GLV Voltage */
  }
}
void nextionTestLoop(void)
{
  switch(actual_page)
  {
    case PAGE0:

    NexNumberSetValue(0, auxiliar);
    NexProgressBarSetValue(0, auxiliar);
    NexProgressBarSetValue(1, auxiliar);
    break;
    
    case PAGE1:

    NexNumberSetValue(0, auxiliar);
    NexNumberSetValue(1, auxiliar);
    NexNumberSetValue(2, auxiliar);
    NexTextSetText(0, "aberto");
    NexTextSetText(1, "aberto");
    NexTextSetText(2, "aberto");
    break;

    case PAGE2:

    NexNumberSetValue(0, auxiliar);
    NexNumberSetValue(3, auxiliar);
    NexNumberSetValue(1, auxiliar);
    //if(air_status == 0) NexTextSetText(0, "aberto");
    //else  NexTextSetText(0, "fechado");
    NexNumberSetValue(2, auxiliar);
    NexXfloatSetValue(0, auxiliar);
  }

  //VARIAVEIS PARA TESTE:
  if(auxiliar == 99)	auxiliar = 0;
  else	auxiliar++;
}

/* Other Functions */

void UART_Print_Debug(char* format, ...)
{
	char buffer[100];
	uint8_t size = 0;
	buffer[0] = '\0';

	va_list argList;

	va_start(argList, format);
	size = vsprintf(buffer, format, argList);

	HAL_UART_Transmit(&huart1, (uint8_t *)buffer, size, 10);

	va_end(argList);
}

void interfaceInit(void)
{
  /* UART DMA Init */
	USART_DMA_Init(&huart2, &hdma_usart2_rx);
  USART_DMA_Init(&huart3, &hdma_usart3_rx);

  /* CAN Init */
  canConfigFilter();
	canConfigFrames();
	canReceive();

  /* can data vector init */
  for(uint16_t i = 0; i < CAN_IDS_NUMBER; i++){
    can_vector[i].word_0 = 0;
    can_vector[i].word_1 = 0;
    can_vector[i].word_2 = 0;
    can_vector[i].word_3 = 0;
  }

  /* Requesting real time */
  realClockRequest(); /* 1.5s Delay */

  /* Nextion Init */
  nexInit();

  /* Global timer variables init */
  packTimer.previous = HAL_GetTick();
  packTimer.interval = 1000;
  ledTimer.previous = HAL_GetTick();
  ledTimer.interval = 50;
}

void canMessageReceived(uint16_t id, uint8_t* data)
{
  blinkLed();
  uint16_t* data_word = (uint16_t*)data;
  can_vector[id].word_0 = data_word[0];
  can_vector[id].word_1 = data_word[1];
  can_vector[id].word_2 = data_word[2];
  can_vector[id].word_3 = data_word[3];
}

void blinkLed(void)
{
  if((actualTimer-ledTimer.previous)>ledTimer.interval)
  {
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    ledTimer.previous = HAL_GetTick();
  }
} /* Debug Led */

void timerAtualization(void)
{
  actualTimer = HAL_GetTick();
}

void debugFunction(void)
{
  UART_Print_Debug("Real Clock: %u : %u\r\n", can_vector[REAL_CLK_CAN_ID].word_2, can_vector[REAL_CLK_CAN_ID].word_3);
  HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
  HAL_Delay(1000);
}
