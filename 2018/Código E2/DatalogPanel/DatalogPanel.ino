/*
Datalog - Panel - UART

This code was made with the intention of receiving strings
by the communication protocol uart that will be stored in a microsd card and shown in a panel.

modified 02 january 2019
Author: Guilherme VinÃ­cius Amorim
Interface Formula Tesla UFMG
*/

//Includes:
#include <SD.h>
#include "panel.h"

//Defines:
#define PIN_LED            2

//Global Variables:

//Leds Variables:
unsigned long currentLedMillis, previousLedMillis, ledInterval = 100;
int ledValue = LOW;

//Panel Variables:
unsigned int media_speed = 0, media_temperature = 0, charge_percent = 0, state_AIR = 0;
byte panelRx[8], indexPanelRx = 0;

void setup() {
  PANEL_Init();
  LED_Init(2);
  while(1)
  {
    delay(20);
    I2C_Transmit(LED_SPEED_TEN_ADDR , B10000011);
    I2C_Transmit(LED_SPEED_UNITY_ADDR, B00110001);
    I2C_Transmit(LED_TEMP_ADDR, B11100000);
    I2C_Transmit(LED_CHARGE_ADDR, B00000000);
  }
}

void loop()
{
}

//Functions:
void LED_Init(int pinLed)
{
  pinMode(pinLed, OUTPUT);
  digitalWrite(pinLed, HIGH);
}

void blinkLed(int pinLed)
{
  if((currentLedMillis-previousLedMillis)>ledInterval)
  {
    if(ledValue == LOW)  ledValue = HIGH;
    else  ledValue = LOW;
    digitalWrite(pinLed, ledValue);
    previousLedMillis = millis();
  }
}

