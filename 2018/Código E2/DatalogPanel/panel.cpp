#include "panel.h"

//Global Variables:
byte ActualCharge, ActualTemp;  //Variables to not send repeated informations to panel
boolean actualTenPercntLed, actualTwentyPercntLed;  //Variables to store if theese leds are on or off
const byte tenPercntInterval = 50, twentyPercntInterval = 200; //Interval to blink theese leds
unsigned long currentMillis, previousTenMillis, previousTwentyMillis; //Timer leds variables

//Functions:
void PANEL_Init()
{
     Wire.begin();
     /*//rotina para encontrar slaves on i2c bus
     Serial.println ("I2C scanner. Scanning ...");
     byte count = 00;
     for (byte i = 1; i < 120; i++)
     {
       Wire.beginTransmission (i);
       if (Wire.endTransmission () == 0)
         {
         Serial.print ("Found address: ");
         Serial.print (i, DEC);
         Serial.print (" (0x");
         Serial.print (i, HEX);
         Serial.println (")");
         count++;
         } // end of good response
        delay (5);  // give devices time to recover
     } // end of for loop
     Serial.println ("Done.");
     Serial.print ("Found ");
     Serial.print (count, DEC);
     Serial.println (" device(s).");
      // end of setup*/
      ActualCharge = 110;
      ActualTemp = B11111111;
      actualTenPercntLed = 0;
      actualTwentyPercntLed = 0;
      previousTwentyMillis = millis();
      previousTenMillis = millis();
      currentMillis = millis();
}

void I2C_Transmit(byte address, byte valor)
{
  Wire.beginTransmission(address);
  Wire.write(valor);
  Wire.endTransmission();
}

void PANEL_Charge_Leds_Transmit(unsigned int charge)
{
 if((byte)(charge/10) != ActualCharge)
  {
    ActualCharge = (byte)(charge/10);
    if(charge > 99)
    {
      I2C_Transmit(LED_CHARGE_ADDR, HUDRED_PERCENT);
      return;
    }
    if(charge > 89)
    {
      I2C_Transmit(LED_CHARGE_ADDR, NINETY_PERCENT);
      return;
    }
    if(charge > 79)
    {
      I2C_Transmit(LED_CHARGE_ADDR, EIGHTY_PERCENT);
      return;
    }
    if(charge > 69)
    {
      I2C_Transmit(LED_CHARGE_ADDR, SEVENTY_PERCENT);
      return;
    }
    if(charge > 59)
    {
      I2C_Transmit(LED_CHARGE_ADDR, SIXTY_PERCENT);
      return;
    }
    if(charge > 49)
    {
      I2C_Transmit(LED_CHARGE_ADDR, FIFTY_PERCENT);
      return;
    }
    if(charge > 39)
    {
      I2C_Transmit(LED_CHARGE_ADDR, FOURTY_PERCENT);
      return;
    }
    if(charge > 29)
    {
      I2C_Transmit(LED_CHARGE_ADDR, THIRTY_PERCENT);
      return;
    }
    I2C_Transmit(LED_CHARGE_ADDR, TWENTY_PERCENT);
  }
}

void PANEL_Speed_Leds_Transmit(unsigned int speed)
{
  int untySpeed = speed / 10;
  int tenSpeed = speed % 10;
  //byte  unity[10] = {B00010100,B11011110,B00111000,B10011000,B11010010,B10010001,B00010001,B11011100,B00010000,B10010000};
  //byte  ten[10] = {B00000101,B11101101,B10000011,B11000001,B01101001,B01010001,B00010001,B11001101,B00000001,B01000001};
  byte  unity[10] = {UNT_ZERO, UNT_ONE, UNT_TWO, UNT_THREE, UNT_FOUR, UNT_FIVE, UNT_SIX, UNT_SEVEN, UNT_EIGHT, UNT_NINE};
  byte  ten[10] = {TEN_ZERO, TEN_ONE, TEN_TWO, TEN_THREE, TEN_FOUR, TEN_FIVE, TEN_SIX, TEN_SEVEN, TEN_EIGHT, TEN_NINE};
  I2C_Transmit(LED_SPEED_TEN_ADDR , ten[tenSpeed]);
  I2C_Transmit(LED_SPEED_UNITY_ADDR, unity[untySpeed]);
}

void PANEL_Temp_Leds_Transmit(unsigned int temperature, unsigned int charge, unsigned int air)
{
  byte aux = B00000000;

  //Bit 7:
  if(temperature < 55)
    aux |= (1 << BIT_7);
  //Bit 6:
  if(temperature < 45)
    aux |= (1 << BIT_6);
  //Bit 5:
  if(temperature < 35)
    aux |= (1 << BIT_5);
  //Bit 4:
  if(temperature < 25)
    aux |= (1 << BIT_4);
  //Bit3:
    aux |= (1 << BIT_3);
  //Bit 2:
  if(air == 0)
    aux |= (1 << BIT_2);
  //Bit1:
  if(charge < 20)
  {
    if(charge > 10)
    {
      if((currentMillis-previousTwentyMillis)>twentyPercntInterval)
      {
        if(actualTwentyPercntLed == 0)
        {
          aux |= (1 << BIT_1);
          actualTwentyPercntLed = 1;
          previousTwentyMillis = millis();
        }
        else  actualTwentyPercntLed = 0;
      }
    }
    else
    {
      if((currentMillis-previousTenMillis)>tenPercntInterval)
      {
        if(actualTenPercntLed == 0)
        {
          aux |= (1 << BIT_1);
          actualTenPercntLed = 1;
          previousTenMillis = millis();
        }
        else  actualTenPercntLed = 0;
      }
    }
  }
  //Bit 0:
  if(charge < 20)
    aux |= (1 << BIT_0);

  if(aux != ActualTemp)
  {
    I2C_Transmit(LED_TEMP_ADDR, aux);
    ActualTemp = aux;
  }
}

void PANEL_Transmit(unsigned int speed, unsigned int temperature, unsigned int charge, unsigned int air)
{
  PANEL_Charge_Leds_Transmit(charge);
  PANEL_Speed_Leds_Transmit(speed);
  PANEL_Temp_Leds_Transmit(temperature, charge, air);
  currentMillis = millis();
}

/*void PANEL_Temp_Leds_Transmit(int temperature, int charge)
{
  byte aux = B00000000;

  //Bit 7:
  if(temperature < 55)
    aux |= B00000001;
  aux = aux << 1;


  //Bit 6:
  if(temperature < 45)
    aux |= B00000001;
  aux = aux << 1;


  //Bit 5:
  if(temperature < 35)
    aux |= B00000001;
  aux = aux << 1;


  //Bit 4:
  if(temperature < 25)
    aux |= B00000001;
  aux = aux << 1;


  //Bit3:
  aux |= B00000001;
  aux = aux << 1;


  //Bit 2:
  if(state_AIR == 0)
    aux |= B00000001;
  aux = aux << 1;


  //Bit1:
  if(charge == 0)
    aux |= B00000001;
  aux = aux << 1;


  //Bit 0:
  if(charge < 11)
    aux |= B00000001;


  //Serial.println("Ta okei ta okei");
  I2C_Transmit(expansorTemp, aux);
}*/

