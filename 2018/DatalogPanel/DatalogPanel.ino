/*
Datalog - Panel - UART

This code was made with the intention of receiving strings
by the communication protocol uart that will be stored in a microsd card and shown in a panel.

modified 26 march 2019
Author: Guilherme Viní­cius Amorim
Interface Formula Tesla UFMG
*/

//Includes:
#include <SD.h>


//Defines:
#define END_CHARACTER      '\n'
#define DATALOG_CHARACTER  '&'
#define PIN_LED            2
#define SD_SS              10 //Sd Select Slave

//Global Variables:
char fileName[10];
boolean _messageComplete = false;
boolean _datalogInformation = false;

//Leds Variables:
unsigned long currentLedMillis, previousLedMillis, ledInterval = 100;
int ledValue = LOW;

//Datalog Variables:
unsigned int vetIntDatalog[10];
byte datalogRx[20];
char datalogString[128];

void setup() {
  LED_Init(PIN_LED);
  SD.begin(SD_SS);
  SD_CreateFiles();
  Serial.begin(57600);
}

void loop()
{
  UART_Check_Message();

  if(_messageComplete)
  {
    if(_datalogInformation)
    {
      File dataFile = SD.open(fileName, FILE_WRITE);
      if(dataFile)
      {
        blinkLed(PIN_LED);
        datalogConvertByte2Int();

        byte len = 0;
        len = sprintf(datalogString, "%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%d\t%u\n",
        vetIntDatalog[0], vetIntDatalog[1], vetIntDatalog[2],
        vetIntDatalog[3], vetIntDatalog[4], vetIntDatalog[5],
        vetIntDatalog[6], vetIntDatalog[7], (int)vetIntDatalog[8],
        vetIntDatalog[9]);

        dataFile.println(datalogString);
        dataFile.close();
        }
        _datalogInformation = false;
      }
      _messageComplete = false;
    }
    currentLedMillis = millis();
  }

//Functions:
void SD_CreateFiles()
{
  byte i = 0;
  sprintf(fileName, "ARQ%02d.txt", i);
  while(SD.exists(fileName))
  {
    i++;
    sprintf(fileName, "ARQ%02d.txt", i);
  }
}

void LED_Init(int pinLed)
{
  pinMode(pinLed, OUTPUT);
  digitalWrite(pinLed, HIGH);
}

void blinkLed(int pinLed)
{
  if((currentLedMillis-previousLedMillis)>ledInterval)
  {
    if(ledValue == LOW)  ledValue = HIGH;
    else  ledValue = LOW;
    digitalWrite(pinLed, ledValue);
    previousLedMillis = millis();
  }
}

void datalogConvertByte2Int()
{
  byte j = 0;
  for(byte i = 0; i < 10; i++)
  {
    vetIntDatalog[i] = *((unsigned int*)(datalogRx + j));
    j+=2;
  }
}

void UART_Check_Message()
{
  byte indexDatalogRx = 0;
  while(Serial.available())
  {
    if(_datalogInformation)
    {
      byte inByte = (byte)Serial.read();
      if((char)inByte == END_CHARACTER)
        _messageComplete = true;
      else
      {
        datalogRx[indexDatalogRx] = inByte;
        if(indexDatalogRx < 19) indexDatalogRx++;
        else  indexDatalogRx = 0;
      }
    }
    else
    {
      char inChar = (char)Serial.read();
      if(inChar == DATALOG_CHARACTER)
        _datalogInformation = true;
    }
  }
}


/*void UART_Check_Message()
{
  while(Serial.available())
  {
    if(_datalogInformation)
    {
      byte inByte = (byte)Serial.read();
      datalogRx[indexDatalogRx] = inByte;
      if((char)inByte == "\n" || indexDatalogRx > 35)
      {
        indexDatalogRx = 0;
        _messageComplete = true;
      }
      else indexDatalogRx++;
    }
    else
    {
      char inChar = (char)Serial.read();
      switch(inChar)
      {
        case END_CHARACTER:
          _messageComplete = true;
          break;
        case PANEL_CHARACTER:
          _panelInformation = true;
          break;
        case DATALOG_CHARACTER:
          _datalogInformation = true;
      }
     }
   }
  }
}*/
