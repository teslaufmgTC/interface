#ifndef PANEL_H_
#define PANEL_H_

#include <Wire.h>
#include "Arduino.h"

//Leds Addres:
#define LED_CHARGE_ADDR             0x38
#define LED_SPEED_TEN_ADDR          0x3B
#define LED_SPEED_UNITY_ADDR        0x3A
#define LED_TEMP_ADDR               0x39

//Charge Leds:
#define HUDRED_PERCENT             B00000000
#define NINETY_PERCENT             B10000000
#define EIGHTY_PERCENT             B11000000
#define SEVENTY_PERCENT            B11010000
#define SIXTY_PERCENT              B11110000
#define FIFTY_PERCENT              B11111000
#define FOURTY_PERCENT             B11111100
#define THIRTY_PERCENT             B11111110
#define TWENTY_PERCENT             B11111111

//Unity Speed Leds:
#define UNT_ZERO                  B00010100
#define UNT_ONE                   B11011110
#define UNT_TWO                   B00111000
#define UNT_THREE                 B10011000
#define UNT_FOUR                  B11010010
#define UNT_FIVE                  B10010001
#define UNT_SIX                   B00010001
#define UNT_SEVEN                 B11011100
#define UNT_EIGHT                 B00010000
#define UNT_NINE                  B10010000

//Ten Speed Leds:
#define TEN_ZERO                  B00000101
#define TEN_ONE                   B11101101
#define TEN_TWO                   B10000011
#define TEN_THREE                 B11000001
#define TEN_FOUR                  B01101001
#define TEN_FIVE                  B01010001
#define TEN_SIX                   B00010001
#define TEN_SEVEN                 B11001101
#define TEN_EIGHT                 B00000001
#define TEN_NINE                  B01000001

//Temperature Byte(Bit Map):
#define BIT_7                   7
#define BIT_6                   6
#define BIT_5                   5
#define BIT_4                   4
#define BIT_3                   3
#define BIT_2                   2
#define BIT_1                   1
#define BIT_0                   0

//Functions:
void PANEL_Init();
void I2C_Transmit(byte address, byte valor);
void PANEL_Charge_Leds_Transmit(unsigned int charge);
void PANEL_Speed_Leds_Transmit(unsigned int speed);
void PANEL_Temp_Leds_Transmit(unsigned int temperature, unsigned int charge, unsigned int air);
void PANEL_Transmit(unsigned int speed, unsigned int temperature, unsigned int charge, unsigned int air);

#endif /* PANEL_LIB_H_ */

