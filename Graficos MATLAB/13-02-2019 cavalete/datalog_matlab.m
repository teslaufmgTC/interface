clear
clc
close all

fid = fopen('ARQ1.txt');
s = textscan(fid, '%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d', 'headerlines', 23);
fclose(fid);

ecu_timer = s{1};
media_speed = s{2};
mean_torque = s{3};
shuttlecock = s{4};
pedal_break = s{5};
inv_energy_right = s{6};
inv_energy_left = s{7};
motor_current_right = s{8};
motor_current_left = s{9};
temp1_inv_right = s{10};
temp2_inv_right = s{11};
temp1_inv_left = s{12};
temp2_inv_left = s{13};
media_current = s{14};
total_voltage = s{15};
media_temperature = s{16};
max_temperature = s{17};
min_voltage = s{18};

figure(1)

subplot(6, 1, 1)
plot(media_speed)
title('MEDIA SPEED')
xlabel('time')
ylabel('media_speed')

subplot(6, 1, 2)
plot(mean_torque)
title('MEAN TORQUE')
xlabel('time')
ylabel('mean_torque')

subplot(6, 1, 3)
plot(shuttlecock)
title('SHUTTLECOCK')
xlabel('time')
ylabel('shuttlecock')

subplot(6, 1, 4)
plot(pedal_break)
title('PEDAL BREAK')
xlabel('time')
ylabel('pedal_break')

subplot(6, 1, 5)
plot(inv_energy_right)
title('INVERSOR ENERGY RIGHT')
xlabel('time')
ylabel('inv_energy_right')

subplot(6, 1, 6)
plot(inv_energy_left)
title('INVERSOR ENERGY LEFT')
xlabel('time')
ylabel('inv_energy_left')

