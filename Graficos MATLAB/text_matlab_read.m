clear
clc
close all

fid = fopen('ARQ0.txt')
s = textscan(fid, '%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d', 'headerlines', 23)
fclose(fid)

media_speed = s{2};
min_voltage = s{18};
temp_media = s{16};
total_voltage = s{15};

figure(1)

subplot(2, 2, 1)
plot(media_speed)
title('MEDIA SPEED')
xlabel('time')
ylabel('media_speed')

subplot(2, 2, 2)
plot(min_voltage)
title('MIN VOLTAGE')
xlabel('time')
ylabel('min_voltage')

subplot(2, 2, 3)
plot(temp_media)
title('TEMP MEDIA')
xlabel('time')
ylabel('temp_media')

subplot(2, 2, 4)
plot(total_voltage)
title('TOTAL VOLTAGE')
xlabel('time')
ylabel('total_voltage')
