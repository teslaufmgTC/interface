import React, { Component } from 'react';
import FieldJanela from './ComponentesJanela.js'
// import logo from './logo.svg';
import logo from './LOGO\ FUNDO\ BRANCO.png'
import './App.css';


const delay = 20;

var lastLoaded = new Date().getMilliseconds();

class App extends Component {
  state = {
    data: {
      portconnected: false,
      bms: {
        currents:           [],
        glv:                null,
        charge:             null,
        operationmode:      null,
        errorflag:          null,
        meancurrent:        null,
        totalvoltage:       null,
        meantemperature:    null,
        maxtemperature:     null,
        minvoltage:         null,
        airstatus:          null,
        packs:              [
            {
                cells: [],
                temperatures: [],
                errors: []
            },
            {
                cells: [],
                temperatures: [],
                errors: []
            },
            {
                cells: [],
                temperatures: [],
                errors: []
            },
            {
                cells: [],
                temperatures: [],
                errors: []
            }
        ]
      },

      control: {
          currentevent:   null,
          eventstart:     null,
          eventended:     null,
          ecuTimer:       null,
          steringWheel:   null,
          speed: {
              frontright: null,
              frontleft:  null,
              backright:  null,
              backleft:   null
          },
          engine: {
              torque: {
                  rightengine:    null,
                  leftengine:     null,
                  expectedright:  null,
                  expectedleft:   null,
                  meantorque:     null
              },
              current: {
                  right: null,
                  left:  null
              }
          },
          
          pedals: {
              throttle:   null,
              brake:      null
          },
          meanspeed: null,
          inverters: {
              energy: {
                  right:  null,
                  left:   null
              },
              temperature: {
                  right1: null,
                  right2: null,
                  left1:  null,
                  left2:  null
              }
          }
      }
    }
  }
  


  

  componentDidMount() {
    //console.log("did mount")
    this.timerID = setInterval(
      () => this.updateField()
        .then(res => 
          this.setState({
            data: res.data
          })
        ).catch(err => console.log(err)),
      delay
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  updateField = async () => {
    const response =  await fetch('/express_backend');
    const body = await response.json();
    if (response.status !== 200) {
      throw Error(body.message) 
    }
    // console.log(new Date().getMilliseconds()-lastLoaded);
    // lastLoaded = new Date().getMilliseconds();
    //console.log(body);
    return body;
  }

  render() {
    // debugger;
    var cells = this.state.data.bms.packs.map((pack, indexp) => 
      <div key={indexp} className="col-12 col-md-3 mb-3">
        <div className="card">
          <div className="card-header">
            <div className="row">
              <h2 className="col text-center">PACK {indexp+1}:</h2>
            </div>
          </div>
          <ul className="card-list-group list-group-flush" style={{"-webkit-padding-start": '0px'}}>
            <li className="list-group-item">
              <div className="row no-gutters justify-content-center">
                {pack.cells.map((cell, indexc) => 
                  <div key={indexp.toString()+"-"+indexc.toString()} className="col-3">
                    <Cell value={cell}></Cell>
                  </div>
                )}
              </div>
            </li>
            <li className="list-group-item">
              <div className="row no-gutters justify-content-center">
                {pack.temperatures.map((temperature, indext) =>
                  <div key={indexp.toString()+"-"+indext.toString()} className="col-3">
                    <Temperature value={temperature}></Temperature>
                  </div>
                  )}
              </div>
            </li>
          </ul>
        </div>
      </div>
    );

    return (
      <div className="container-fluid">
        <div className="row mt-3">
          {/* <div className="col-12 d-flex justify-content-center align-items-center">
            <div className="image-container">
              <img src={logo} alt="Logo"></img>
            </div>
            <span className="display-2">Janela de Dados</span>
          </div> */}
          <div className="col-6 col-lg mb-3">
            <OutlinedField title="Speed:" value={this.state.data.control.meanspeed/1000.0} message="km/h"></OutlinedField>
          </div> 
          
          <div className="col-6 col-lg mb-3">
            <OutlinedField title="Mean Torque:" value={this.state.data.control.engine.torque.meantorque/1000} message=""></OutlinedField>
          </div>
          <div className="col-6 col-lg mb-3">
            <OutlinedField title="GLV voltage:" value={this.state.data.bms.glv/1000.0} message="V"></OutlinedField>
          </div>
          <div className="col-6 col-lg mb-3">
            <OutlinedField title="Mean Temperature:" value={this.state.data.bms.meantemperature/1000} message="ºC"></OutlinedField>
          </div>
          <div className="w-100"></div>
          <div className="col-md-12">
            <label>Baterias:</label>
          </div>
          <div className="col-12">
            <div className="row">
              <ComponentInputGroupField title="Max T" value={this.state.data.bms.maxtemperature/1000.0}></ComponentInputGroupField>
              <ComponentInputGroupField title="Min V" value={this.state.data.bms.minvoltage/10000.0}></ComponentInputGroupField>
              <ComponentInputGroupField title="Total V" value={this.state.data.bms.totalvoltage/100.0}></ComponentInputGroupField>
              <ComponentInputGroupField title="Mean C" value={this.state.data.bms.meancurrent/1000.0}></ComponentInputGroupField>
            </div>
          </div>
          <div className="col-md-12">
            <div className="row justify-content-center">
              {cells} 
            </div>
          </div>
          <div className="col-12">
            <div className="row">
              {this.state.data.bms.currents.map((current, index) => 
                <div className="col-6 col-lg mb-3" key={index}>
                  <InputGroupField title="Current" value={current > (Math.pow(2, 8)-1) ? Math.abs((current-(Math.pow(2, 16)-1))/10000.0): Math.abs(current/10000.0) }></InputGroupField>
                </div>
              ) }
            </div>
          </div>
          <div className="col-md-12">
            <div className="bs-example">
              <label>Engines:</label>
              <div className="row">
                <div className="col">
                  <div className="card">
                    <div className="card-header">
                      <h3 className="card-title">Left</h3>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-12 col-md-6">
                          Torque:
                          <FieldJanela value={this.state.data.control.engine.torque.leftengine/1000} message=""></FieldJanela>
                        </div>
                        <div className="col-12 col-md-6">
                          Expected torque:
                          <FieldJanela value={this.state.data.control.engine.torque.expectedleft/1000} message=""></FieldJanela>
                        </div>
                        <div className="col-12 col-md-6">
                          Inverter T1:
                          <FieldJanela value={this.state.data.control.inverters.temperature.left1/10.0} message="ºC"></FieldJanela>
                        </div>
                        <div className="col-12 col-md-6">
                          Inverter T2:
                          <FieldJanela value={this.state.data.control.inverters.temperature.left2/10.0} message="ºC"></FieldJanela>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="card">
                    <div className="card-header">
                      <h3 className="card-title">Right</h3>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-12 col-md-6">
                          Torque:
                          <FieldJanela value={this.state.data.control.engine.torque.rightengine/1000} message=""></FieldJanela>
                        </div>
                        <div className="col-12 col-md-6">
                          Expected torque:
                          <FieldJanela value={this.state.data.control.engine.torque.expectedright/1000} message=""></FieldJanela>
                        </div>
                        <div className="col-12 col-md-6">
                          Inverter T1:
                          <FieldJanela value={this.state.data.control.inverters.temperature.right1/10.0} message="ºC"></FieldJanela>
                        </div>
                        <div className="col-12 col-md-6">
                          Inverter T2:
                          <FieldJanela value={this.state.data.control.inverters.temperature.right2/10.0} message="ºC"></FieldJanela>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="row">
              <ComponentInputGroupField title="Throttle" value={this.state.data.control.pedals.throttle}></ComponentInputGroupField>
              <ComponentInputGroupField title="Brake" value={this.state.data.control.pedals.brake}></ComponentInputGroupField>
              <ComponentInputGroupField title="Steering" value={this.state.data.control.steringWheel}></ComponentInputGroupField>
              <ComponentInputGroupField title="Speed fl" value={this.state.data.control.speed.frontleft}></ComponentInputGroupField>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
  
export default App;


class Cell extends Component {


  render() {
    return (
      <div>
        <LimitedInformationText value={this.props.value} diviser={10000} upperBound={3.65} lowerBound={2.8}></LimitedInformationText>
      </div>
    );
  }
}

class Temperature extends Component {

  render() {
    return (
      <div>
        <LimitedInformationText value={this.props.value} diviser={1000} upperBound={55} lowerBound={0}></LimitedInformationText>
      </div>
    );
  }
}

class LimitedInformationText extends Component {

  render() {
    var value = this.props.value / this.props.diviser;

    var labelClass = "limitedtext text-center" + (value > this.props.upperBound ? " over-voltage" : value < this.props.lowerBound ? " under-voltage" : "");
    return (
      <div>
        <label className={labelClass}>
          {value}
        </label>
      </div>
    );
  }

}


class OutlinedField extends Component {
  render() {
    return (
      <div className="outlined">
        <span className="lead"><b>{this.props.title}</b></span>
        <FieldJanela value={this.props.value} message={this.props.message}></FieldJanela>
      </div>
    )
  }
}

class ComponentInputGroupField extends Component {
  render() {
    return (
      <div className="col-6 col-lg mb-3">
        <InputGroupField title={this.props.title} value={this.props.value}></InputGroupField>
      </div>
    )
  }
}

class InputGroupField extends Component {
  render() {
    return (
      <div className="input-group">
        <div className="input-group-prepend">
          <span className="input-group-text">{this.props.title}</span>
        </div>
        <input type="text" readOnly className="form-control readonly-g" value={this.props.value}></input>
      </div> 
    );
  }
}

