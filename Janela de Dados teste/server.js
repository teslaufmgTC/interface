const ObjectCreator = require('./object_creator.js');

const express = require('express');
const fs = require('fs');




const filepath = './datalog/datalog.txt'
var fd;

const app = express();
const port = process.env.PORT || 5000;
//const serialport = require('serialport');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline')

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));

SerialPort.list(function (err, ports) {
  ports.forEach(function(port) {
    console.log(port.comName);
    console.log(' - pnpId: ' + port.pnpId);
    console.log(' - manufacturer: ' + port.manufacturer);
    console.log(' - serialNumber: ' + port.serialNumber);
    console.log(' - vendorId: ' + port.vendorId);
    console.log(' - productId: ' + port.productId);
  });
});




var comport = "/dev/tty.SLAB_USBtoUART"
var comport = "/dev/tty.usbmodem14101"

var content;

try {
  content = fs.readFileSync("./porta.txt", 'utf8');
} catch (e) {
  
}

console.log(content);

var ttl_port;


var recentInformation = new ObjectCreator();

const readLine = SerialPort.parsers.Readline


const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));


async function openPort() {
  console.log("Esperando conexão da porta...");
  var found = false;
  while (!found) {
    
    await sleep(1000);
    var ports = await SerialPort.list();

    if (ports.length == 0)
      continue;
    
    for(var i = 0; i<ports.length; i++) {
      console.log("Tentando porta: "+ports[i].comName);
      if (content != undefined && content !== "") {
        if (ports[i].comName !== content) {
          continue;
        }
      } else if (ports[i].comName == "/dev/tty.Bluetooth-Incoming-Port")
        continue;
      
      ttl_port = new SerialPort(ports[i].comName, { baudRate: 115200, autoOpen: false })
      var opened = false;
      var error;
      ttl_port.open(async err => {
        error = err;
        
        if (!err) {
          var closed = false;
          ttl_port.close(() => closed = true);
          while(!closed)
            await sleep(5);
        }

        opened = true;
          
      });
      //await new Promise(resolve => )
      while(!opened)
          await sleep(10);
      //se tiver erro, fecha a porta e a seta como nulo
      if (error) {
        console.log("erro ao abrir a porta "+ttl_port.path+": "+errr);
        ttl_port = undefined
      } else {
        found = true;
        break;
      }
      
    }
      /*.then(ports => {
        if (ports.length <= 1) {
          return false;
        }
        for(var i = 0; i<ports.length; i++) {
          if (ports[i].comName == "/dev/tty.Bluetooth-Incoming-Port")
            continue;
          else {
            ttl_port = new SerialPort(ports[i].comName, { baudRate: 115200, autoOpen: false })
            var error = false;
            var errr;
            ttl_port.open(err => {
              errr = err;
              if (err)
                error = true;
              else 
                ttl_port.close();
            });
            //se tiver erro, fecha a porta e a seta como nulo
            if (error) {
              console.log("erro ao abrir a porta "+ttl_port.path+": "+errr);
              ttl_port.close();
              ttl_port = undefined
            } else {
              found = true;
              break;
            }
          }
        }
        // if (ttl_port === undefined) {
        //   console.log("Porta não encontrada!");
        //   process.exit(-1);
        // }
      },
      err => {
        console.log("Erro ao listar portas!");
        console.log("Referência: ", err);
        process.exit(-1);
      })*/
    }
}

async function initSerialPort() {
    await openPort();
    ttl_port.open(err => {});
    ttl_port.on("open", function () {
      console.log('open');
      recentInformation.setPortConnected(true);
      ttl_port.on("close", () => {
        console.log("port closed");
        recentInformation.setPortConnected(false);
        ttl_port = undefined;
        parser = undefined;
        initSerialPort();
      })
    })
      
    
    var parser = ttl_port.pipe(new readLine());
    parser.on('data', dataReceived)
    
  }

initSerialPort();
//const ttl_port = new SerialPort(comport, { baudRate: 115200 })


function dataReceived(data) {
  //console.log('new data');
  var newData = data.toString().split('\t');
  //console.log(newData);
  var id = parseInt(newData[0], 10);

  


  recentInformation.treatInfo(id, newData.slice(1)).then( () => {
    // var now = new Date().getMilliseconds();
    // console.log(now - lastEscrito);
    // lastEscrito = now;
  });
  // var now = new Date().getMilliseconds();
  // console.log(now - lastEscrito);
  // lastEscrito = now;
  // console.log(recentInformation.getInfo());
  if(id == 0x0)
    writeFile().catch(err => console.error(err));
}

var lastEscrito=new Date().getMilliseconds();

var shouldWrite = true;

async function writeFile() {
  if (shouldWrite) {
    var now = new Date();




    fs.appendFile(filepath, recentInformation.buildDatalog(), err => {
      if(err)
        console.error(err);
      //var now = new Date().getMilliseconds();
      //console.log(now - lastEscrito);
      //lastEscrito = now;
    });
  }
}

// var ttl_port = new serialport(comport, {
//   baudRate: 115200,
//   parser: new serialport.parsers.Readline('\n')
// });





// ttl_port.on('data', function(data) {
//   //console.log(data);
//   var newData = data.toString().split('\t');
//   console.log(newData);
//   var id = parseInt(data[0], 16);
//   recentInformation.treatInfo(id, newData.slice(1));
// });

// create a GET route
app.get('/express_backend', (req, res) => {
  res.send({data: recentInformation.getInfo()});
});


process.on('SIGTERM', () => {
  app.close(() => {
    console.log('Process terminated')
  })
  if (ttl_port) {
    ttl_port.close(() => {
      console.log('Porta fechada')
    })
  }
})

//res.send(recentInformation.getInfo());